module.exports = function (grunt) {
	'use strict';
    // Project configuration
    grunt.initConfig({
        server: {
            port: 8080,
            base: '.'
        }       
    });

    // Default task
    grunt.registerTask('default',['server']);
};