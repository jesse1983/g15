(function() {
  var GaiaApp;

  GaiaApp = angular.module('GaiaApp', ['ngResource', 'ngAnimate', 'ngRoute', 'ngSanitize', 'StringFilters']);

  angular.module('GaiaApp').config(function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('!');
    return $routeProvider.when("/", {
      templateUrl: "views/dashboard/index.html",
      controller: "DashboardController"
    }).when("/properties", {
      templateUrl: "views/properties/index.html",
      controller: "PropertiesController"
    }).when("/gaia_university", {
      templateUrl: "views/gaia_university/index.html",
      controller: "GaiaUniversityController"
    });
  });

}).call(this);
