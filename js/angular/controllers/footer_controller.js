(function() {
  angular.module('GaiaApp').controller("FooterController", function($scope, ContactPhonesFactory, MenuFactory) {
    $scope.openedSupport = false;
    ContactPhonesFactory.query({}, function(response) {
      return $scope.contact_phones = response;
    });
    MenuFactory.get({}, function(response) {
      return $scope.footer = response;
    });
    $scope.currentPhone = 0;
    $scope.isCurrentPhone = function(index) {
      return $scope.currentPhone === index;
    };
    $scope.setCurrentPhone = function(index) {
      if (index >= $scope.contact_phones.length) {
        return $scope.currentPhone = 0;
      } else {
        return $scope.currentPhone = index;
      }
    };
    return $scope.getNextPhone = function() {
      var index;
      if ($scope.contact_phones) {
        index = $scope.currentPhone + 1;
        if (index >= $scope.contact_phones.length) {
          return $scope.contact_phones[0];
        } else {
          return $scope.contact_phones[index];
        }
      }
    };
  });

}).call(this);
