angular.module('GaiaApp').controller "FooterController",($scope,ContactPhonesFactory, MenuFactory) ->
	$scope.openedSupport = false

	ContactPhonesFactory.query {}, (response) ->
		$scope.contact_phones = response
	MenuFactory.get {}, (response) ->
		$scope.footer = response

	$scope.currentPhone = 0

	$scope.isCurrentPhone = (index) ->
		($scope.currentPhone == index)

	$scope.setCurrentPhone = (index) ->
		if index >= $scope.contact_phones.length
			$scope.currentPhone = 0
		else
			$scope.currentPhone = index
	$scope.getNextPhone = () ->
		if $scope.contact_phones
			index = $scope.currentPhone + 1
			if index >= $scope.contact_phones.length
				$scope.contact_phones[0]
			else
				$scope.contact_phones[index]


	

