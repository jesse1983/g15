(function() {
  angular.module('GaiaApp').controller("HeadController", function($scope, HeadFactory) {
    $scope.theme = "default";
    return HeadFactory.get({}, function(response) {
      $scope.head = response;
      if ($scope.head.theme) {
        return $scope.theme = $scope.head.theme;
      }
    });
  });

}).call(this);
