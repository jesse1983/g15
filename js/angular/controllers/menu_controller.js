(function() {
  angular.module('GaiaApp').controller("MenuController", function($scope, $sce, $timeout, MenuFactory, NotificationsFactory) {
    $scope.show_search = false;
    $scope.loading = true;
    $scope.toogleNoti = function(bool) {
      return $scope.show_alert = bool;
    };
    $scope.setSearchBy = function(item) {
      return $scope.searchBy = item;
    };
    $scope.clearNotifications = function() {
      return $scope.unread_notifications = [];
    };
    return MenuFactory.get({}, function(response_menu) {
      $scope.main_menu = response_menu.main_menu;
      $scope.home_image_path = response_menu.home_image_path;
      $scope.secondary_menu = response_menu.secondary_menu;
      $scope.search_options = response_menu.search_options;
      $scope.searchBy = $scope.search_options[0];
      $scope.loading = false;
      if (response_menu.head_notifications) {
        $scope.alert = response_menu.head_notifications;
        $scope.alert.content = $sce.trustAsHtml($scope.alert.content);
        $timeout(function() {
          return $scope.toogleNoti(true);
        }, $scope.alert.start);
        $timeout(function() {
          return $scope.toogleNoti(false);
        }, $scope.alert.end);
      }
      return NotificationsFactory.query({}, function(response_notifications) {
        return $scope.unread_notifications = response_notifications;
      });
    });
  });

}).call(this);
