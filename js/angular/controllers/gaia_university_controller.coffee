angular.module('GaiaApp').controller "GaiaUniversityController",($scope,$http,$resource) ->
	Course = $resource "json/courses.json", {}
	Course.query {}, (data) ->
		$scope.courses = data

	$scope.register_me = (att, obj) ->
		if att
			$http.post(obj.register_path, {}).success (data) ->
				obj.registered = att		
		else
			$http.post(obj.unregister_path, {}).success (data) ->
				obj.registered = att		

		
		