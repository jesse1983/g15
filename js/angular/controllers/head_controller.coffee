angular.module('GaiaApp').controller "HeadController",($scope, HeadFactory) ->
	$scope.theme = "default"
	HeadFactory.get {},(response) ->
		$scope.head = response
		$scope.theme = $scope.head.theme if $scope.head.theme
