(function() {
  angular.module('GaiaApp').controller("GaiaUniversityController", function($scope, $http, $resource) {
    var Course;
    Course = $resource("json/courses.json", {});
    Course.query({}, function(data) {
      return $scope.courses = data;
    });
    return $scope.register_me = function(att, obj) {
      if (att) {
        return $http.post(obj.register_path, {}).success(function(data) {
          return obj.registered = att;
        });
      } else {
        return $http.post(obj.unregister_path, {}).success(function(data) {
          return obj.registered = att;
        });
      }
    };
  });

}).call(this);
