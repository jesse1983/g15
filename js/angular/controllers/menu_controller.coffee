angular.module('GaiaApp').controller "MenuController",($scope, $sce, $timeout, MenuFactory, NotificationsFactory) ->
	$scope.show_search = false
	$scope.loading = true

	$scope.toogleNoti = (bool)->
		$scope.show_alert = bool

	$scope.setSearchBy = (item) ->
		$scope.searchBy = item
		
	$scope.clearNotifications = ->
		$scope.unread_notifications = []


	MenuFactory.get {}, (response_menu) ->		
		$scope.main_menu = response_menu.main_menu
		$scope.home_image_path = response_menu.home_image_path
		$scope.secondary_menu = response_menu.secondary_menu
		$scope.search_options = response_menu.search_options
		$scope.searchBy = $scope.search_options[0]
		$scope.loading = false
		if response_menu.head_notifications
			$scope.alert = response_menu.head_notifications
			$scope.alert.content = $sce.trustAsHtml $scope.alert.content
			$timeout () -> 
				$scope.toogleNoti(true)
			, $scope.alert.start
			$timeout () -> 
				$scope.toogleNoti(false)
			, $scope.alert.end

		NotificationsFactory.query {}, (response_notifications) ->
			$scope.unread_notifications = response_notifications



