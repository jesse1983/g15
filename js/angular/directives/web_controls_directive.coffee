



angular.module('GaiaApp').directive 'spinner', () ->
	restrict: 'E'
	template: '
	<style>#fadingBarsG{position:relative;width:200px;height:24px; margin:auto;}.fadingBarsG{position:absolute;top:0;background-color:#FFF;width:24px;height:24px;-moz-animation-name:bounce_fadingBarsG;-moz-animation-duration:.7s;-moz-animation-iteration-count:infinite;-moz-animation-direction:linear;-moz-transform:scale(.3);-webkit-animation-name:bounce_fadingBarsG;-webkit-animation-duration:.7s;-webkit-animation-iteration-count:infinite;-webkit-animation-direction:linear;-webkit-transform:scale(.3);-ms-animation-name:bounce_fadingBarsG;-ms-animation-duration:.7s;-ms-animation-iteration-count:infinite;-ms-animation-direction:linear;-ms-transform:scale(.3);-o-animation-name:bounce_fadingBarsG;-o-animation-duration:.7s;-o-animation-iteration-count:infinite;-o-animation-direction:linear;-o-transform:scale(.3);animation-name:bounce_fadingBarsG;animation-duration:.7s;animation-iteration-count:infinite;animation-direction:linear;transform:scale(.3)}#fadingBarsG_1{left:0;-moz-animation-delay:.28s;-webkit-animation-delay:.28s;-ms-animation-delay:.28s;-o-animation-delay:.28s;animation-delay:.28s}#fadingBarsG_2{left:25px;-moz-animation-delay:.35s;-webkit-animation-delay:.35s;-ms-animation-delay:.35s;-o-animation-delay:.35s;animation-delay:.35s}#fadingBarsG_3{left:50px;-moz-animation-delay:.42s;-webkit-animation-delay:.42s;-ms-animation-delay:.42s;-o-animation-delay:.42s;animation-delay:.42s}#fadingBarsG_4{left:75px;-moz-animation-delay:.49s;-webkit-animation-delay:.49s;-ms-animation-delay:.49s;-o-animation-delay:.49s;animation-delay:.49s}#fadingBarsG_5{left:100px;-moz-animation-delay:.56s;-webkit-animation-delay:.56s;-ms-animation-delay:.56s;-o-animation-delay:.56s;animation-delay:.56s}#fadingBarsG_6{left:125px;-moz-animation-delay:.63s;-webkit-animation-delay:.63s;-ms-animation-delay:.63s;-o-animation-delay:.63s;animation-delay:.63s}#fadingBarsG_7{left:150px;-moz-animation-delay:.7s;-webkit-animation-delay:.7s;-ms-animation-delay:.7s;-o-animation-delay:.7s;animation-delay:.7s}#fadingBarsG_8{left:175px;-moz-animation-delay:.77s;-webkit-animation-delay:.77s;-ms-animation-delay:.77s;-o-animation-delay:.77s;animation-delay:.77s}@-moz-keyframes bounce_fadingBarsG{0%{-moz-transform:scale(1);background-color:#FFF}100%{-moz-transform:scale(.3);background-color:#FFF}}@-webkit-keyframes bounce_fadingBarsG{0%{-webkit-transform:scale(1);background-color:#FFF}100%{-webkit-transform:scale(.3);background-color:#FFF}}@-ms-keyframes bounce_fadingBarsG{0%{-ms-transform:scale(1);background-color:#FFF}100%{-ms-transform:scale(.3);background-color:#FFF}}@-o-keyframes bounce_fadingBarsG{0%{-o-transform:scale(1);background-color:#FFF}100%{-o-transform:scale(.3);background-color:#FFF}}@keyframes bounce_fadingBarsG{0%{transform:scale(1);background-color:#FFF}100%{transform:scale(.3);background-color:#FFF}}</style>
	<div id="fadingBarsG">
		<div id="fadingBarsG_1" class="fadingBarsG"></div>
		<div id="fadingBarsG_2" class="fadingBarsG"></div>
		<div id="fadingBarsG_3" class="fadingBarsG"></div>
		<div id="fadingBarsG_4" class="fadingBarsG"></div>
		<div id="fadingBarsG_5" class="fadingBarsG"></div>
		<div id="fadingBarsG_6" class="fadingBarsG"></div>
		<div id="fadingBarsG_7" class="fadingBarsG"></div>
		<div id="fadingBarsG_8" class="fadingBarsG"></div>	
	</div>
	'