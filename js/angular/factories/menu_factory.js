(function() {
  angular.module('GaiaApp').factory('MenuFactory', [
    '$resource', function($resource) {
      return $resource("json/menu.json");
    }
  ]);

}).call(this);
