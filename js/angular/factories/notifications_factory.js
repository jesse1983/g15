(function() {
  angular.module('GaiaApp').factory('NotificationsFactory', [
    '$resource', function($resource) {
      return $resource("json/notifications.json");
    }
  ]);

}).call(this);
