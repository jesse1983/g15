(function() {
  angular.module('GaiaApp').factory('HeadFactory', [
    '$resource', function($resource) {
      return $resource("json/head.json");
    }
  ]);

}).call(this);
