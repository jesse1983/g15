angular.module('GaiaApp').factory 'NotificationsFactory', ['$resource', ($resource) ->
	$resource "json/notifications.json"
]   