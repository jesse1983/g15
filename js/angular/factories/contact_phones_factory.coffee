angular.module('GaiaApp').factory 'ContactPhonesFactory', ['$resource', ($resource) ->
	$resource "json/contact_phones.json"
]   