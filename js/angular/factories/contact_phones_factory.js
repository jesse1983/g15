(function() {
  angular.module('GaiaApp').factory('ContactPhonesFactory', [
    '$resource', function($resource) {
      return $resource("json/contact_phones.json");
    }
  ]);

}).call(this);
