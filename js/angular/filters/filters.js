(function() {
  angular.module('StringFilters', []).filter('dateTime', function() {
    return function(input, date_expression) {
      var mons, myData;
      myData = new Date(input);
      mons = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
      if (date_expression === "day") {
        if (parseInt(myData.getDay()) < 10) {
          return "0" + (myData.getDay());
        } else {
          return "" + (myData.getDay());
        }
      } else if (date_expression === "month") {
        return mons[myData.getMonth()];
      } else if (date_expression === "hours") {
        return myData.getHours();
      } else if (date_expression === "minutes") {
        if (parseInt(myData.getMinutes()) < 10) {
          return "0" + (myData.getMinutes());
        } else {
          return "" + (myData.getMinutes());
        }
      }
    };
  });

}).call(this);
