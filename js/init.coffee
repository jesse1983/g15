GaiaApp = angular.module 'GaiaApp', ['ngResource','ngAnimate','ngRoute','ngSanitize','StringFilters']

angular.module('GaiaApp').config ($routeProvider, $locationProvider) ->
	# $locationProvider.html5Mode true
	$locationProvider.hashPrefix '!'  		
	$routeProvider
		.when "/",
			templateUrl: "views/dashboard/index.html"
			controller: "DashboardController"
		.when "/properties",
			templateUrl: "views/properties/index.html"
			controller: "PropertiesController"
		.when "/gaia_university",
			templateUrl: "views/gaia_university/index.html"
			controller: "GaiaUniversityController"			



